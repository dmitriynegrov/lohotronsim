use std::collections::HashSet;
use std::fmt::{Display, Formatter};
use rand::{Rng, seq::SliceRandom, thread_rng, seq::index::sample};
use num_traits::NumCast;
use statrs::distribution::{Binomial, Discrete};
use rayon::prelude::*;

const NUM_ACTCOLS: usize = 5;
const NUM_GROUPS: usize = 10;
const NUM_ROWS: usize = 6;
const N_TESTS: usize = 1_000_000;
const SAMPLE_SIZE: usize = 12;
const DRAW_SIZE: usize = 10_000_000;
const LOSS_NUMS: usize = 3;


#[derive(Eq, PartialEq, Debug, Hash)]
struct Ticket (Vec<Vec<u8>>);
struct Draw (Vec<Ticket>);

impl Ticket {
    fn generate_row_base<R: Rng>(rng: &mut R) -> Vec<Vec<u8>> {
        let mut ret = Vec::new();

        for _ in 0..NUM_ROWS {
            let mut arr: Vec<u8> = (0..(NUM_GROUPS as u8)).collect();
            arr.shuffle(rng);
            arr.truncate(NUM_ACTCOLS);
            ret.push(arr);
        }

        ret
    }

    fn populate_rows<R: Rng>(rng: &mut R, bases: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
        let mut masks: Vec<u32> = vec![0; NUM_GROUPS];

        for (row, c) in bases.iter().enumerate() {
            for n in c {
                masks[*n as usize] |= 1 << row;
            }
        }

        let mut ret: Vec<Vec<u8>> = vec![Vec::new(); NUM_ROWS];

        for (id, m) in masks.iter().enumerate() {
            let rows = m.count_ones();
            if rows == 0 {
                continue;
            }
            if rows > 10 {
                panic!("To many rows during generation");
            }
            let mut arr: Vec<u8> = (0..10).collect();
            arr.shuffle(rng);
            let mut m_loc = *m;

            let mut row = 0;
            while m_loc != 0 {
                if m_loc & 1 != 0 {
                    ret[row as usize].push((id*10) as u8 + arr[row]);
                }
                m_loc >>= 1;
                row += 1;

            }
        }

        ret

    }

    fn new<R: Rng>(rng: &mut R) -> Self {
        let rb = Ticket::generate_row_base(rng);

        Ticket(Ticket::populate_rows(rng, rb))
    }

    fn has_num(&self, num: u8) -> bool {
        for x in &self.0 {
            for y in x {
                if num == *y {
                    return true;
                }
            }
        }
        false
    }

    fn has_nums(&self, nums: &[u8]) -> bool{
        nums.iter().all(|val| self.has_num(*val))
    }

    fn has_at_leas_one_num(&self, nums: &[u8]) -> bool{
        nums.iter().any(|val| self.has_num(*val))
    }
}

impl Display for Ticket {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", "-".repeat(5*10 + 1))?;
        for row in &self.0 {
            let mut idx: usize = 0;
            for ord in 0..10 {
                if idx != row.len() && row[idx] / 10 == ord {
                    write!(f, "| {:2} ", row[idx])?;
                    idx += 1;
                }
                else {
                    write!(f, "|    ")?;
                }
            }
            writeln!(f, "|")?;
            //write!(f, "{}\n", "-".repeat(5*10 + 1));
        }
        writeln!(f, "{}", "-".repeat(5*10 + 1))

    }
}

fn generate_draw() -> Draw
{
    let mut ret: HashSet<Ticket> = (0..DRAW_SIZE)
        .into_par_iter()
        .map(|_| Ticket::new(&mut thread_rng()))
        .collect();

    while ret.len() < DRAW_SIZE {
        let addition = (0..(DRAW_SIZE - ret.len()))
            .into_par_iter()
            .map(|_|
                Ticket::new(&mut thread_rng()));
        ret.extend(addition.collect::<HashSet<_>>());
    }

    Draw(ret.into_iter().collect())
}

fn generate_unique_seq<R: Rng, T: NumCast>(rng: &mut R, max: usize, n: usize) -> Vec<T> {
    sample(rng, max, n).into_iter().map(|x| T::from(x).unwrap()).collect()
}

fn generate_draw_sample<'a, R: Rng>(rng: &mut R, draw: &'a Draw, size: usize) -> Vec<&'a Ticket>{
    let seq: Vec<usize> = generate_unique_seq(rng, draw.0.len(), size);

    let mut ret = Vec::new();

    for n in seq {
        ret.push(&draw.0[n]);
    }

    ret
}

fn get_sample_losses(sample: &[&Ticket], tgt_nums: &[u8]) -> usize {
    sample.iter().filter(|t| t.has_at_leas_one_num(tgt_nums)).count()
}

fn main() {
    let mut rng = thread_rng();

    println!("Generating draw");
    let draw = generate_draw();
    println!("Complete.");

    let tgt_nums: Vec<u8> = generate_unique_seq(&mut rng, 100, LOSS_NUMS);
    //let tgt_nums= vec![17, 66, 76];
    println!("Testing against {:?}", tgt_nums);

    let empirical_distribution: Vec<usize> = (0..N_TESTS).into_par_iter()
        .map(|_| {
            let sample = generate_draw_sample(&mut thread_rng(), &draw, SAMPLE_SIZE);
            get_sample_losses(&sample, &tgt_nums)
        })
        .map(|val| {
            let mut v = vec![0; SAMPLE_SIZE+1];
            v[val] = 1;
            v
        })
        .reduce(|| vec![0; SAMPLE_SIZE+1],
                  |hist, right| hist.iter().zip(&right).map(|(x, y)| x + y).collect());

    let n_hits: usize = empirical_distribution.iter().enumerate().fold(0, |s, x| s + x.0*x.1);

    let fail_prob = n_hits as f64 / ((SAMPLE_SIZE*N_TESTS) as f64);
    println!("Done. Total hits: {} / {}", n_hits, SAMPLE_SIZE*N_TESTS);
    println!("Loss probability: {}", fail_prob);
    println!("Tested against {:?}", tgt_nums);

    let distr = Binomial::new(fail_prob, SAMPLE_SIZE as u64).unwrap();
    println!("PMF for Bernoulli sampling:");

    let mut total: f64 = 0.;
    let mut etotal: f64 = 0.;

    println!("Loses\tProbability\tEmpirical");
    for (successes, ed) in empirical_distribution.iter().enumerate() {
        let prob = distr.pmf(successes as u64);
        let eprob = *ed as f64 / N_TESTS as f64;
        total += prob;
        etotal += eprob;
        println!("{}\t\t{:2.2}%\t\t{:2.2}%", successes, prob*100., eprob * 100.);
    }
    println!("Total:\t{:2.2}%\t\t{:2.2}%", total*100., etotal*100.);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_gen_correctness() {
        let mut rng = thread_rng();

        for _ in 0..100 {
            let t = Ticket::new(&mut rng);
            let mut s: HashSet<u8> = HashSet::new();
            for x in &t.0 {
                for y in x {
                    s.insert(*y);
                }
            }

            println!("{:?}", t);
            assert_eq!(s.len(), NUM_ACTCOLS * NUM_ROWS);
        }
    }
}